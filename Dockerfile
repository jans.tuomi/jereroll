FROM node:slim AS builder
WORKDIR /app
EXPOSE 8080
COPY package.json package-lock.json ./
RUN npm ci

FROM node:slim as runner
WORKDIR /app
COPY --from=builder /app/node_modules node_modules
COPY . ./
CMD ["npm", "start"]
