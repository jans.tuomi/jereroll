const path = require('path');
const express = require('express');
const app = express();

app.use('/static', express.static(path.join(__dirname, 'public')))
app.set('view engine', 'ejs');

const getRandomInt = max => Math.floor(Math.random() * Math.floor(max));

app.get('/:roll', (req, res) => {
    const [n, sides] = req.params.roll.split('j');
    let total = 0;
    for (let i = 0; i < n; i++) {
        total += getRandomInt(sides) + 1;
    }

    res.render('index', { count: total });
});

app.get('/', (req, res) => {
    res.send('Try /XjY');
});

app.listen(8080);
console.log('Serving on http://localhost:8080');

